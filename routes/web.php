<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Route::get('/service-job', function () {

//    dispatch(new CheckAllegroCompetitorsJob(\App\Models\Url::find(3)));

//    dispatch(new \App\Jobs\ServiceParserInboxRpcJob([
//        [
//            'rpc' => 2.0,
//            'method' => 'urlAdd',
//            'params' => [
//                'uuid' => 'fh67sfdj8',
//                'url' => 'https://allegro.pl/kategoria/laptopy-apple-77915?string=macbook%20pro%2015&bmatch=cl-e2101-d3681-c3682-ele-1-5-0319',
//                'type' => 'allegro-url',
//                'reply_to' => 'test',
//            ],
//            'id' => 1
//        ]
//    ]));
//});

//Route::get('/allegro-job', function () {
//    $url = \App\Models\Url::find(3);
//    dispatch(new \App\Jobs\ParseAllegroLinkJob($url, false));
//});
//
//Route::get('/allegro-competitors', function () {
//    $url = \App\Models\Url::find(1);
//    dispatch(new \App\Jobs\AllegroCompetitors($url));
//});
//
//Route::get('/allegro', function () {
////    $token = AllegroRestApi::generateTokenForApplication( config( 'allegro.client_id'), config( 'allegro.client_secret') );
////    dd($token);
//
//    $parser = new \App\Services\AllegroParser();
//
//    $client = new AllegroRestApi(null, null);
//    $token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJhbGxlZ3JvOmFwaTpvcmRlcnM6cmVhZCIsImFsbGVncm86YXBpOnByb2ZpbGU6d3JpdGUiLCJhbGxlZ3JvOmFwaTpzYWxlOm9mZmVyczp3cml0ZSIsImFsbGVncm86YXBpOmJpbGxpbmc6cmVhZCIsImFsbGVncm86YXBpOmNhbXBhaWducyIsImFsbGVncm86YXBpOmRpc3B1dGVzIiwiYWxsZWdybzphcGk6c2FsZTpvZmZlcnM6cmVhZCIsImFsbGVncm86YXBpOmJpZHMiLCJhbGxlZ3JvOmFwaTpvcmRlcnM6d3JpdGUiLCJhbGxlZ3JvOmFwaTphZHMiLCJhbGxlZ3JvOmFwaTpwYXltZW50czp3cml0ZSIsImFsbGVncm86YXBpOnNhbGU6c2V0dGluZ3M6d3JpdGUiLCJhbGxlZ3JvOmFwaTpwcm9maWxlOnJlYWQiLCJhbGxlZ3JvOmFwaTpyYXRpbmdzIiwiYWxsZWdybzphcGk6c2FsZTpzZXR0aW5nczpyZWFkIiwiYWxsZWdybzphcGk6cGF5bWVudHM6cmVhZCJdLCJhbGxlZ3JvX2FwaSI6dHJ1ZSwiZXhwIjoxNjE2MDYzMDk2LCJqdGkiOiIyNTkxNGU2OC0yNGMzLTQ2OWYtODgzMy02M2Y3YTA5OGQ0MjUiLCJjbGllbnRfaWQiOiI5MTJkMDg1NGI1NmQ0MzZlYTY1YTdlZTljZjc5OWE0YSJ9.O3jiRKne9NgKCnNw17mfqNcpFm6upgpupGSOzeBef17hA785AdywIemd8vZopnsxxCIp73kse9giQ_KHC-crsPn567Ux1HkCm-NdRRw47Ansj9-kOrbs4DtYn2t0ZseQqKfCkvs7IaMcx7Q8kY4PUy7nBvO8eoCZVbfvYiu4M1sOmzZ0RWLIWwjY6O0TJiSHVtstb1wKoQr23D8dfMHCmbYmoyMrEe79iLau_QO9i0X-z8GBShmYirDSG6lAfyEKwTddwC7eL5adMgoKOuYW_WByLs30MX5200hgve2T_GsrJuA49RQq0fr1gjvfRlpgWdP_XdjvOpeSAd4cLwgXQQ';
//
//    $parser->setClient( $client, $token );
////    $parser->setUrl('https://allegro.pl/listing?string=macbook%20air&price_from=200&price_to=2000&offerTypeBuyNow=1&stan=nowe&stan=u%C5%BCywane&dostawa-kurier=1&bmatch=cl-e2101-d3720-c3682-ele-1-4-0304');
//    $parser->setUrl('https://allegro.pl/kategoria/laptopy-apple-77915?string=macbook%20air&price_from=200&price_to=2000&offerTypeBuyNow=1&stan=nowe&stan=u%C5%BCywane&dostawa-kurier=1&bmatch=cl-e2101-d3720-c3682-ele-1-4-0304');
//
//    $counter = 0;
//    $parser->parse( function( $results ) use (&$counter) {
//        foreach ( $results as $result ) {
//            $counter++;
//
//            $data = [
//                'remote_id' => $result->id,
//                'title' => $result->name,
//                'description' => '',
//                'price' => $result->sellingMode->price->amount,
//                'quantity' => $result->stock->available,
//            ];
//
//            $images = [];
//            foreach ($result->images as $image ) {
//                $images[] = $image->url;
//            }
//            $data['photos'] = implode(',', $images );
//
//            dd($data);
////            $parsing->results()->create($data);
//
//        }
//    });
//});
