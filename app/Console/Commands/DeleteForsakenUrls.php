<?php

namespace App\Console\Commands;

use App\Models\Url;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DeleteForsakenUrls extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'urls:forsaken:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Start ' . $this->signature);
        Url::where('is_active', 0)->where('updated_at', '<', Carbon::now()->subDays(30))->chunkById(100, function ($urls) {
            foreach ($urls as $url) {
                $url->delete();
            }
        });
        $this->info('Finish ' . $this->signature);
    }
}
