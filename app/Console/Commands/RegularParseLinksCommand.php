<?php

namespace App\Console\Commands;

use App\Jobs\ParseAllegroLinkJob;
use App\Models\Url;
use Illuminate\Console\Command;

class RegularParseLinksCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'urls:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (config('allegro_parsing_mode') == 'ALLEGRO_API') {
            $this->info('Start ' . $this->signature);
            Url::where('is_active', 1)->chunkById(100, function ($urls) {
                foreach ($urls as $url) {
                    dispatch(new ParseAllegroLinkJob($url, false));
                }
            });
            $this->info('Finish ' . $this->signature);
        }
    }
}
