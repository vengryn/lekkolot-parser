<?php

namespace App\Jobs;

use App\Models\Url;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class CheckAllegroCompetitorsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The url instance.
     *
     * @var \App\Models\Url
     */
    protected $link;

    /**
     * @var int
     */
    protected $count;

    /**
     * Create a new job instance.
     *
     * @param \App\Models\Url $link
     * @return void
     */
    public function __construct(Url $link)
    {
        $this->link = $link;
        $this->count = 0;

        if (config('queue.default', 'sync') === 'rabbitmq') {
            $this->onQueue('service_parser__default');
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Log::channel('jobs-allegro-competitors-stat')->info('[' . $this->link->id . '] Start search competitors');

            $checkedParsing = $this->link->parsings()
                ->with(['parsingResults'])
                ->orderByDesc('checked_at')
                ->first();

            $this->link->competitors()->detach();

            if ($checkedParsing) {
                $newParsing = $this->link->parsings()
                    ->with(['parsingResults'])
                    ->whereNull('checked_at')
                    ->where('id', '>', $checkedParsing->id)
                    ->latest()
                    ->first();

                if ($newParsing) {
                    $checkedParsingResult = empty($checkedParsing->checked_at)
                        ? collect() : $checkedParsing->parsingResults->keyBy('data.remote_id');
                    $newParsingResult = $newParsing->parsingResults->keyBy('data.remote_id');

                    $diffOffers = $newParsingResult->pluck('data.price', 'data.remote_id')
                        ->diffAssoc($checkedParsingResult->pluck('data.price', 'data.remote_id'))
                        ->keys()
                        ->filter(function ($item) use ($checkedParsingResult, $newParsingResult) {
                            if (isset($checkedParsingResult[$item])) {
                                if (isset($newParsingResult[$item]->data['price']) && isset($checkedParsingResult[$item]->data['price'])) {
                                    return $newParsingResult[$item]->data['price'] < $checkedParsingResult[$item]->data['price'];
                                } else {
                                    return false;
                                }
                            }
                            return true;
                        });

                    $result = $newParsing->parsingResults->whereIn('data.remote_id', $diffOffers);

                    $this->link->competitors()->sync($result->pluck('id')->toArray());
                    $this->count = $result->count();

                    $lastParse = $this->link->parsings->sortByDesc('created_at')->first();

                    dispatch(new ServiceParserClientInboxJob([
                        'jsonrpc' => '2.0',
                        'result' => [
                            'url_uuid' => $this->link->uuid,
                            'parsing_id' => $lastParse->id ?? null,
                            'parsing_date' => $lastParse->created_at ?? null,
                            'competitors_count' => $this->link->competitors->count()
                        ],
                        'id' => 'allegro-competitors-result'
                    ]))->onQueue($this->link->reply_to_queue ?: 'default');

                    $this->link->parsings()
                        ->where('created_at', '>', $checkedParsing->created_at)
                        ->where('created_at', '<', $newParsing->created_at)
                        ->delete();
                }

                $this->link->parsings()
                    ->where('created_at', '<', $checkedParsing->created_at)
                    ->delete();
            }

            Log::channel('jobs-allegro-competitors-stat')->info('[' . $this->link->id . '] Finished search competitors. Total: ' . $this->count);

        } catch (\Exception $exception) {
            Log::error($exception->getMessage(), ['link competitors' => $this->link]);
            $this->fail($exception);
        }
    }
}
