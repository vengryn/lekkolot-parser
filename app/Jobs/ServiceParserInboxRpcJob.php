<?php

namespace App\Jobs;

use App\Models\Parsing;
use App\Models\Url;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ServiceParserInboxRpcJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $data;

    /**
     * Create a new job instance.
     *
     * @param array $data
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Log::channel('jobs-parser-inbox-rpc-stat')->info('Start parser inbox rpc');

            if ($this->isMulti($this->data)) {
                foreach ($this->data as $data) {
                    $this->run($data['method'], $data['params']);
                }
            } else {
                $this->run($this->data['method'], $this->data['params']);
            }

            Log::channel('jobs-parser-inbox-rpc-stat')->info('Finished parser inbox rpc');

        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            $this->fail($exception);
        }
    }

    protected function run($method, $params)
    {
        try {
            switch ($method) {
                case 'urlAdd':

                    $url_params = [];

                    if (!empty($params["exclude_remote_id"])) $url_params['exclude_remote_id'] = $params["exclude_remote_id"];

                    $link = Url::updateOrCreate(['uuid' => $params['uuid']], [
                        'url' => $params['url'],
                        'type' => $params['type'],
                        'reply_to_queue' => $params['reply_to'],
                        'params' => $url_params,
                    ]);

                    $is_checked = $params['is_checked'] ?? true;

                    if (config('allegro_parsing_mode') == 'ALLEGRO_API') {
                        dispatch(new ParseAllegroLinkJob($link, $is_checked));
                    }

                    break;
                case 'urlRemove':
                    if ($link = Url::where('uuid', $params['uuid'])->first()) {
                        $link->delete();
                    }
                    break;
                case 'urlParsingApprove':
                    if ($parsing = Parsing::with('url')->find($params['parsing_id'])) {
                        $parsing->checked_at = Carbon::now();
                        $parsing->save();

                        Parsing::where('url_id', $parsing->url_id)
                            ->where('id', '<', $parsing->id)
                            ->delete();

                        dispatch(new CheckAllegroCompetitorsJob($parsing->url));
                    }
                    break;
                case 'urlSuspendParsing':
                    if ($link = Url::where('uuid', $params['uuid'])->first()) {
                        $link->is_active = false;
                        $link->save();
                    }
                    break;
                case 'urlResumeParsing':
                    if ($link = Url::where('uuid', $params['uuid'])->first()) {
                        $link->is_active = true;
                        $link->save();
                    }
                    break;
            }
        } catch (\Exception $e) {
            report($e);
            throw $e;
        }

    }

    protected function isMulti($array)
    {
        return isset($array[0]);
    }
}
