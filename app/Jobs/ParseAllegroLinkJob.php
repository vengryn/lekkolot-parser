<?php

namespace App\Jobs;

use App\Models\Parsing;
use App\Models\ParsingResult;
use App\Models\Url;
use App\Services\AllegroParser;
use AsocialMedia\AllegroApi\AllegroRestApi;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class ParseAllegroLinkJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 600;

    /**
     * The url instance.
     *
     * @var \App\Models\Url
     */
    protected $link;

    /**
     * @var bool
     */
    protected $is_checked;

    /**
     * @var int
     */
    protected $count;

    /**
     * Create a new job instance.
     *
     * @param \App\Models\Url $link
     * @param boolean $isChecked
     * @return void
     */
    public function __construct(Url $link, bool $isChecked)
    {
        $this->link = $link;
        $this->is_checked = $isChecked;
        $this->count = 0;

        $this->onConnection( 'rabbitmq' )->onQueue( 'service_parser__default' );
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $parsing = $this->link->parsings()->create();

        try {
            Log::channel('jobs-allegro-parse-stat')->info( '[' .$parsing->id . '] Start parsing');

            $client = new AllegroRestApi(null, null);

            $token =  Cache::remember( 'allegro_token', 21600, function(){
                return AllegroRestApi::generateTokenForApplication( config( 'allegro.client_id'), config( 'allegro.client_secret') );
            });

            $parser = new AllegroParser();
            $parser->setClient( $client, $token->access_token );
            $parser->setUrl($this->link->url);

            $parser->parse( function( $results ) use ($parsing) {
                $saveData = [];
                foreach ( $results as $result ) {
                    $this->count++;

                    $images = [];
                    $data = [
                        'remote_id' => $result->id,
                        'title' => $result->name,
                        'description' => '',
                        'price' => $result->sellingMode->price->amount,
                        'quantity' => $result->stock->available,
                    ];

                    foreach ( $result->images as $image ) {
                        $images[] = $image->url;
                    }

                    $data['photos'] = implode(',', $images );

                    $saveData[] = [
                        'parsing_id' => $parsing->id,
                        'data' => json_encode($data, JSON_UNESCAPED_UNICODE),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ];
                }
                ParsingResult::query()->insert($saveData);
            });

            if ($this->is_checked) {
                $parsing->update(['checked_at' => Carbon::now()]);
            }

            dispatch( new CheckAllegroCompetitorsJob($this->link) );

            Log::channel('jobs-allegro-parse-stat')->info('[' .$parsing->id . '] Finished catalog parsing. Total: ' . $this->count);

        } catch (\Exception $exception) {
            Log::error( $exception->getMessage(), ['link parse' => $this->link ]);
            $parsing->delete();
            $this->fail( $exception );
        }
    }
}
