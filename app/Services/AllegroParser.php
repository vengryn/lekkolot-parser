<?php

namespace App\Services;

use AsocialMedia\AllegroApi\AllegroRestApi;

class AllegroParser
{
    protected $url;
    protected $token;
    private $client;

    /**
     * @param \AsocialMedia\AllegroApi\AllegroRestApi $client
     * @param $token
     */
    public function setClient( AllegroRestApi $client, $token )
    {
        $this->client = $client;
        $this->client->setToken( $token );
    }

    public function setUrl( string $url )
    {
        $this->url = $url;
    }

    /**
     * @param $url
     * @param string $argSeparator
     * @param int $decType
     * @return array
     */
    private function extractParams($url, $argSeparator = '&', $decType = PHP_QUERY_RFC1738) : array {
        $url_parts = parse_url( $url );
        $queryString = $url_parts['query'];

        $result = [];
        $parts = explode($argSeparator, $queryString);

        foreach ($parts as $part) {
            list($paramName, $paramValue)   = explode('=', $part, 2);

            switch ($decType) {
                case PHP_QUERY_RFC3986:
                    $paramName = rawurldecode($paramName);
                    $paramValue = rawurldecode($paramValue);
                    break;

                case PHP_QUERY_RFC1738:
                default:
                    $paramName = urldecode($paramName);
                    $paramValue = urldecode($paramValue);
                    break;
            }

            if (preg_match_all('/\[([^\]]*)\]/m', $paramName, $matches)) {
                $paramName = substr($paramName, 0, strpos($paramName, '['));
                $keys = array_merge(array($paramName), $matches[1]);
            } else {
                $keys = array($paramName);
            }

            $target = &$result;

            foreach ($keys as $index) {
                if ($index === '') {
                    if (isset($target)) {
                        if (is_array($target)) {
                            $intKeys = array_filter(array_keys($target), 'is_int');
                            $index = count($intKeys) ? max($intKeys)+1 : 0;
                        } else {
                            $target = array($target);
                            $index  = 1;
                        }
                    } else {
                        $target         = array();
                        $index          = 0;
                    }
                } elseif (isset($target[$index]) && !is_array($target[$index])) {
                    $target[$index] = array($target[$index]);
                }

                $target = &$target[$index];
            }

            if (is_array($target)) {
                $target[] = $paramValue;
            } else {
                $target = $paramValue;
            }
        }

        preg_match( '#\/kategoria\/.*-(\d{3,10})#', $url_parts["path"], $category );

        if( !empty($category[1]) ) $result['category.id'] = $category[1];

        $result['offerTypeBuyNow'] = 1;

        return $result;
    }

    /**
     * @param callable $callback
     * @throws \Exception
     */
    public function parse( callable $callback )
    {
        $params = $this->extractParams( $this->url );
        $limit = 60;

        $response = $this->client->get( '/offers/listing?'.http_build_query(
                array_merge(
                    [
                        'category.id' => null,
                        'phrase' => $params['string'],
                        'seller.id' => null,
                        'searchMode' => 'REGULAR',
                        'offset' => 0,
                        'limit' => $limit,
                        'sort' => 'relevance',
                        'fallback' => false
                    ], $params
                )
            )
        );

        if (!empty($response->searchMeta)) {
            $availableCount = (int) $response->searchMeta->availableCount;
        } else{
            throw new \Exception( "Allegro request error. Response:\r\n\r\n " .json_encode( $response ) ."\r\n\r\n" );
        }

        $offers = array_merge($response->items->regular, $response->items->promoted);

        $callback($offers);
        $processed = count($offers);

        for( ; $availableCount > $processed; $processed += count($offers) ) {
            $break = false;

            if ($processed + $limit >= $availableCount) {
                $limit = $availableCount - $processed;
                if ($limit === 0) break;
                $break = true;
            }

            $response = $this->client->get( '/offers/listing?'.http_build_query(
                    array_merge(
                        [
                            'category.id' => null,
                            'phrase' => $params['string'],
                            'seller.id' => null,
                            'searchMode' => 'REGULAR',
                            'offset' => $processed,
                            'limit' => $limit,
                            'sort' => 'relevance',
                            'fallback' => false
                        ], $params
                    )
                )
            );

            if (empty($response->searchMeta)) {
                throw new \Exception( "Allegro request error. Response:\r\n\r\n " .json_encode( $response ) ."\r\n\r\n" );
            }

            $offers = array_merge($response->items->regular, $response->items->promoted);

            $callback($offers);

            if ($break) break;
        }
    }
}
