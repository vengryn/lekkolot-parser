<?php

namespace App\ServerApp\Http\Response;

class JsonRpcResponse
{
    const JSON_RPC_VERSION = '2.0';

    public static function success($result, string $id = null)
    {
        return response([
            'jsonrpc' => self::JSON_RPC_VERSION,
            'result'  => $result,
            'id'      => $id,
        ]);
    }

    public static function error($message, $code)
    {
        return response([
            'jsonrpc' => self::JSON_RPC_VERSION,
            'error'   => $message,
            'id'      => null,
        ], $code);
    }
}
