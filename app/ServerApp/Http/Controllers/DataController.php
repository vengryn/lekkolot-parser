<?php

namespace App\ServerApp\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Jobs\CheckAllegroCompetitorsJob;
use App\Models\Url;
use Carbon\Carbon;

class DataController extends Controller
{
    /**
     * Return competitors for link.
     *
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function showCompetitors(array $params)
    {
        if ($link = Url::where('uuid', $params['uuid'])->first()) {
            $lastParse = $link->parsings->sortByDesc('created_at')->first();

            return [
                'url_uuid' => $link->uuid,
                'url' => $link->url,
                'parsing_id' => $lastParse->id ?? null,
                'parsing_date' => $lastParse->created_at->format('d.m.Y H:i:s') ?? null,
                'competitors_count' => $link->competitors->count(),
                'competitors' => $link->competitors->map(function ($item) {
                    return [
                        'remote_id' => $item->data['remote_id'] ?? null,
                        'remote_url' => $item->data['remote_id'] ? 'https://allegro.pl/oferta/' . $item->data['remote_id'] : null,
                        'title' => $item->data['title'] ?? null,
                        'description' => $item->data['description'] ?? null,
                        'price' => $item->data['price'] ?? null,
                        'quantity' => $item->data['quantity'] ?? null,
                        'photos' => explode(',', $item->data['photos'] ?? [])
                    ];
                })
            ];
        }

        throw new \Exception('Link not found', 400);
    }

    /**
     * Return urls by type.
     *
     * @param array $params
     * @return mixed
     * @throws \Exception
     */
    public function getUrls(array $params)
    {
        if (!empty($params['type'])) {
            return Url::withCount('parsings')
                ->where('type', $params['type'])
                ->orderBy('parsings_count', 'asc')
                ->get();
        }

        throw new \Exception('Type is required', 400);
    }

    /**
     * Create new parsing for link
     *
     * @param array $params
     * @return string[]
     * @throws \Exception
     */
    public function createParsing(array $params)
    {
        if ($link = Url::where('uuid', $params['url_uuid'])->first()) {
            $link->parsings()->create([
                'checked_at' => ($params['is_checked'] ?? false) ? Carbon::now() : null
            ]);

            dispatch(new CheckAllegroCompetitorsJob($link));

            return ['status' => 'success'];
        }

        throw new \Exception('Link not found', 400);
    }
}
