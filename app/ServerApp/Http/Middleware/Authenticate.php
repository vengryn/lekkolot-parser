<?php

namespace App\ServerApp\Http\Middleware;

use App\ServerApp\Http\Response\JsonRpcResponse;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $token = config('app.rpc_control_phrase');

        if (md5($token) != $request->header('token')) {
            return JsonRpcResponse::error('Forbidden', JsonResponse::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
