<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Parsing extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['checked_at'];

    /**
     * The attributes that date.
     *
     * @var array
     */
    protected $dates = ['checked_at'];

    public function url()
    {
        return $this->belongsTo( Url::class );
    }
    /**
     * Get the parsingResults for the parsing.
     */
    public function parsingResults()
    {
        return $this->hasMany(ParsingResult::class);
    }
}
