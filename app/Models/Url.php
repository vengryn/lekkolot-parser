<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['uuid', 'type', 'url', 'is_active', 'reply_to_queue', 'params'];

    protected $casts = [
        'is_active' => 'boolean',
        'params' => 'array'
    ];

    /**
     * Get the parsings for the url.
     */
    public function parsings()
    {
        return $this->hasMany(Parsing::class);
    }

    /**
     * Get the competitors for the url.
     */
    public function competitors()
    {
        return $this->belongsToMany(ParsingResult::class);
    }
}
