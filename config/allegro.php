<?php

return
[
    'sandbox' => env('ALLEGRO_PL_API_IS_SANDBOX', false ),
    'client_id' => env('ALLEGRO_PL_API_CLIENT_ID', null ),
    'client_secret' => env('ALLEGRO_PL_API_CLIENT_SECRET', null ),
    'redirect_url' => env('ALLEGRO_PL_API_REDIRECT_URI', '' ),
    'country' => env( 'ALLEGRO_PL_API_COUNTRY', 1 ),
];
